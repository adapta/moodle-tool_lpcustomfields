<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * List learning plan templates custom fields
 *
 * @package     tool_lpcustomfields
 * @copyright   2023 Daniel Neis Araujo
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/adminlib.php');

admin_externalpage_setup('tool_lpcustomfields_index');

$outputpage = new \tool_lpcustomfields\output\index();

echo $OUTPUT->header(),
     $OUTPUT->heading(new lang_string('fillfields', 'tool_lpcustomfields')),
     $OUTPUT->render($outputpage),
     $OUTPUT->footer();
