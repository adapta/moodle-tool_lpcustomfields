<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     tool_lpcustomfields
 * @category    admin
 * @copyright   2023 Daniel Neis Araujo
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$parentname = 'competencies';

if (get_config('core_competency', 'enabled')) {
    // Manage custom fields for learning plans page.
    $temp = new admin_externalpage(
        'tool_lpcustomfields',
        get_string('pluginname', 'tool_lpcustomfields'),
        new moodle_url('/admin/tool/lpcustomfields/customfield.php'),
        array('moodle/competency:competencymanage')
    );
    $ADMIN->add($parentname, $temp);

    // Populate custom fields for learning plans page.
    $temp = new admin_externalpage(
        'tool_lpcustomfields_index',
        get_string('fillfields', 'tool_lpcustomfields'),
        new moodle_url('/admin/tool/lpcustomfields/index.php'),
        array('moodle/competency:competencymanage')
    );
    $ADMIN->add($parentname, $temp);
}
