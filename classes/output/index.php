<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_lpcustomfields\output;

/**
 * Index renderable
 *
 * @package     tool_lpcustomfields
 * @copyright   2023 Daniel Neis Araujo
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class index implements \renderable, \templatable {

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output
     * @return stdClass
     * @throws coding_exception
     * @throws moodle_exception
     */
    public function export_for_template(\renderer_base $output) {
        global $DB;
        $lp = $DB->get_records('competency_template');
        $handler = \tool_lpcustomfields\customfield\lp_handler::create();
        foreach ($lp as $l) {
            $customfields = $handler->get_instance_data($l->id);
            $a = [
             'shortname' => $l->shortname,
             'editurl' => (new \moodle_url('/admin/tool/lpcustomfields/edit.php', ['id' => $l->id]))->out(false),
             'customfields' => $handler->display_custom_fields_data($customfields)
            ];
            $result[] = $a;
        }
        return (object) ['lp' => $result];
    }
}
