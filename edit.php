<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * List learning plan templates custom fields
 *
 * @package     tool_lpcustomfields
 * @copyright   2023 Daniel Neis Araujo
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/adminlib.php');

$id = required_param('id', PARAM_INT);

admin_externalpage_setup('tool_lpcustomfields');

$form = new \tool_lpcustomfields\form\edit(null, ['id' => $id]);

if ($form->is_cancelled()) {
    redirect(new moodle_url('/admin/tool/lpcustomfields/index.php'));
} else if ($data = $form->get_data()) {
    // Update custom fields if there are any of them in the form.
    $handler = \tool_lpcustomfields\customfield\lp_handler::create();
    $handler->instance_form_save($data);
    $str = get_string('fieldssaved', 'tool_lpcustomfields');
    redirect(new moodle_url('/admin/tool/lpcustomfields/index.php'), $str);
}

$data = $DB->get_record('competency_template', ['id' => $id]);
$handler = \tool_lpcustomfields\customfield\lp_handler::create();
$handler->instance_form_before_set_data($data);
$form->set_data($data);

echo $OUTPUT->header(),
     $OUTPUT->heading(new lang_string('fillfieldsfor', 'tool_lpcustomfields', $data->shortname)),
     $form->render(),
     $OUTPUT->footer();
